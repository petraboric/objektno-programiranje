﻿#include "Counter.hpp"
#include <iostream>
#include <vector>
using namespace std;

int main() {
	vector<Animal*> animal;
	Counter counter;

	animal.push_back(new Zohar);
	animal.push_back(new Vrabac);
	animal.push_back(new Tarantula);

	counter.zbroj_nogu(animal);
	counter.printaj_zbroj();
}