#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Animal {
public:
	virtual int get_legs() = 0;
	virtual string get_species() = 0;
	virtual ~Animal() = 0;
};

class Insect : public Animal {
public:
	Insect() {};
};

class Zohar : public Insect {
private:
	int legs;
	string species;
public:
	Zohar();
	int get_legs();
	string get_species();
	~Zohar();
};

class Bird : public Animal {
public:
	Bird() {};
};

class Vrabac : public Bird {
private:
	int legs;
	string species;
public:
	Vrabac ();
	int get_legs();
	string get_species();
	~Vrabac();
};

class Spider : public Animal {
public:
	Spider() {};
};

class Tarantula : public Spider {
private:
	int legs;
	string species;
public:
	Tarantula();
	int get_legs();
	string get_species();
	~Tarantula();
};

