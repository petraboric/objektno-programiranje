#include "Animal.hpp"

Animal::~Animal() {}

Zohar::Zohar() {
	legs = 6;
	species = "Zohar";
}
int Zohar::get_legs() {
	return legs;
}
string Zohar::get_species() {
	return species;
}
Zohar::~Zohar() {
	legs = 0;
	species = "";
}

Vrabac::Vrabac() {
	legs = 2;
	species = "Vrabac";
}
int Vrabac::get_legs() {
	return legs;
}
string Vrabac::get_species() {
	return species;
}
Vrabac ::~Vrabac() {
	legs = 0;
	species = "";
}

Tarantula::Tarantula() {
	legs = 8;
	species = "Tarantula";
}
int Tarantula::get_legs() {
	return legs;
}
string Tarantula::get_species() {
	return species;
}
Tarantula::~Tarantula() {
	legs = 0;
	species = "";
}

