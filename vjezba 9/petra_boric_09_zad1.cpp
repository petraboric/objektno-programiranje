#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>
#include <iterator>

using namespace std;

bool veci500(int i)
{
	return i>500;
}


bool manji300(int i)
{
	return i< 300;
}

bool silazno(int a, int b)
{
	if (a > b)
		return 1;
	return 0;
}

vector<int> funk(string file)
{
	ifstream fin(file);
	istream_iterator<string> it(fin);
	istream_iterator<string> eos;
	vector<string> v;

	copy(it, eos, back_inserter(v));

	try{
		if (!fin.is_open())
			throw exception();
	}
	catch (exception& e)
	{
		cout << "Error" << endl;
	}

	vector<int> num;
	for (int i = 0; i < v.size(); i++)
	{
		int temp = 0;
		stringstream br(v[i]);
		br >> temp;
		num.push_back(temp);
	}

	
	cout << endl;
	int cnt = count_if(num.begin(), num.end(), veci500);
	cout << cnt << endl;

	int max = *max_element(num.begin(), num.end());
	cout << max << endl;
	int min = *min_element(num.begin(), num.end());
	cout << min << endl;

	num.erase(remove_if(num.begin(),num.end(),manji300), num.end());


	sort(num.begin(), num.end(), silazno);



	

	return num;
}

int main()
{
	vector<int> v;
	string file = "numbers.txt";
	v = funk(file);
	cout << endl;
	for (int i = 0; i < v.size(); i++)
		cout << v[i] << " ";
}