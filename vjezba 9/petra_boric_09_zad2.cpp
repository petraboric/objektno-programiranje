#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>
#include <iterator>
using namespace std;


class less1
{
	int n;
public:
	less1(int l) : n(l) {}
	bool operator()(int br) { return br < n; }
};

bool nepar(int br)
{
	return br % 2 != 0;
}

int main()
{
	vector<int> v = { 22,5,2,6,-1,10,3,-7,2,7,12 };
	cout << count_if(v.begin(), v.end(), less1(10))<<endl;

	vector<int>::iterator it=find_if(v.begin(), v.end(), nepar);
	cout << *it << endl;


	sort(v.begin(), v.end());

	for (it = v.begin(); it < v.end(); it++)
		cout << *it << " ";
	cout << endl;

	for (it = v.begin(); it < v.end(); it++)
		if(*it % 2==0)
			cout << *it << " ";
	for (it = v.begin(); it < v.end(); it++)
		if (*it % 2 != 0)
			cout << *it << " ";
	cout << endl;

}