#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ostream>
#include <fstream>
#include <ctime>
#include <string>
using namespace std;
class exc {
public:
	virtual void getError() = 0;
};

class kriviInput : public exc {
public:
	string str = "Krivi unos ";
	void getError() {
		time_t now = time(0);
		char* dt = ctime(&now);
		cout << str;
		ofstream os("errors.log", std::ios_base::out | std::ios_base::app);
		os << dt + str << '\n';
		os.close();
	}
};
class KriviOperand : public exc {
public:
	string str = "Kriva operacija ";
	void getError() {
		time_t now = time(0);
		char* dt = ctime(&now);
		cout << str;
		ofstream os("errors.log", ios_base::out | ios_base::app);
		os << dt + str << '\n';
		os.close();
	}
};
class Djelisnulom : public exc {
public:
	string str = " Dijeljenje nulom ";
	void getError() {
		time_t now = time(0);
		char* dt = ctime(&now);
		std::cout << str;
		std::ofstream os("errors.log", std::ios_base::out | std::ios_base::app);
		os << dt + str << '\n';
		os.close();
	}
};
int funk_za_broj() {
	cout << "Unesi broj: " << endl;
	int n;
	cin >> n;
	if (cin.fail())
		throw kriviInput();
	return n;
}

char funk_operator() {
	cout << "Unesi operator: " << endl;
	char n;
	cin >> n;
	if (n == '+' || n == '-' || n == '*' || n == '/')
		return n;
	else
		throw KriviOperand();
}

double funk_operacije(int a, int b, char c) {
	if (c == '+')
		return a + b;
	else if (c == '-')
		return a - b;
	else if (c == '*')
		return a * b;
	else if (c == '/')
		if (b == 0)
			throw Djelisnulom();
	return a / b;
}

