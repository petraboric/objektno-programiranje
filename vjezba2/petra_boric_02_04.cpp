using namespace std;
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>


void unosStringova(vector<string>& sniz) {

    string str1;
    int i = 0, velicinaStr;
    cout << "Unesite rijeci\n";
    while (cin >> str1) {    // prekida se ako se upise ctrZ
        velicinaStr = str1.size();
        for (int i = 0; i < velicinaStr / 2; i++) {      // /2
            swap(str1[i], str1[velicinaStr - i - 1]);
        }
        sniz.push_back(str1);
    }
    sort(sniz.begin(), sniz.end());
}

void printajVektor(vector<int>& niz) {

    for (int i = 0; i < niz.size(); i++)
    {
        cout << niz[i] << endl;
    }
}

int main()
{
    vector<string> str;
    unosStringova(str);
    printajVektor(str);
}
