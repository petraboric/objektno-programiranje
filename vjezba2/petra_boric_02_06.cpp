using namespace std;
#include <iostream>
#include <vector>
#include "string"
#include <sstream>
#include <time.h>

void unesivektorRecenice(vector<string>& vector, int n) {                     //stavljanje stringa u vektor dok se ne izvrsi n-gotovo
    for (int i = 0; i < n; i++) {
        cout << "unesi recenice u vektor: ";
        string s;
        getline(cin, s);
        vector.push_back(s);
    }
}

int RandomSentence(int n) {
    srand(time(NULL));
    return rand() % (n + 1);
}

bool isVowel(char s) {
    string vowels = "aeiouAEIOU";
    for (int i = 0; i < 11; i++) {
        if (s == vowels[i]) {
            return true;
        }
    }
    return false;
}

string suglasnikNaKraju(string str) {
    int i = 0;
    while (!isVowel(str[i])) {           //sve dok na kraju nije samoglasnik-i
        i++;
    }
    return str.substr(i) + str.substr(0, i) + "ay";
}

void prevediNaPigLatin(string* sentence) {

    stringstream s(*sentence);         //ubacujem recenicu u s
    string word;   //rijec po rijec
    string pig;    //ukupna recenica na piglatin
    while (s >> word) {
        if (ispunct(word[0])) {                   //puntacija
            pig += word;
        }
        else {
            if (isVowel(word[0])) {
                pig += word + "hay ";
            }
            else {
                pig += suglasnikNaKraju(word) + " ";
            }
        }
    }
    *sentence = pig;
}

int main() {
    vector<string> recenice;
    int broj_recenica;
    cout << "Input number of sentences: ";
    cin >> broj_recenica;
    unesivektorRecenice(recenice, broj_recenica);
    int index = RandomSentence(broj_recenica);
    prevedinnaPigLatin(&recenice[index]);
    printajVektor(recenice, broj_recenica);
}
