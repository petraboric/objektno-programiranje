using namespace std;
#include <iostream>
#include <string>
#include <algorithm>

bool spaces(char a, char b) {
	return ((isspace(a) && isspace(b)));      //akoe tocno-True-radi
}
void ispraviString(string str_no, string& str_yes) {
	auto ip = unique(str_no.begin(), str_no.end(), spaces);   //mice duplikate   //auto samodreduje
	int n = ip - str_no.begin();

	for (int i = 0; i < n; i++) {
		if (isspace(str_no[i]) && ispunct(str_no[i + 1])) {
			str_yes.push_back(str_no[++i]);
			if (isspace(str_no[i + 1])) {
				continue;
			}
			str_yes.push_back(' ');     //stavi razmak
		}
		else if (ispunct(str_no[i]) && isalpha(str_no[i + 1])) {     //provjerava slovo i slovo do njega 
			str_yes.push_back(str_no[i]);
			str_yes.push_back(' ');
		}
		else {
			str_yes.push_back(str_no[i]);
		}
	}

}
int main() {
	cout << "unesi recenicu koju zelis ispraviti: " << endl;
	string str;
	getline(cin, str);   //scanf
	string strcorr = "";
	ispraviString(str, strcorr);
	cout << strcorr;
}
