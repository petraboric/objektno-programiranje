using namespace std;
#include <numeric>
#include <vector>
#include <iostream>
#include <algorithm>


int main(){
    vektor<int> vct = { 6, 88, 56, 23, 18, 3, 55, 43 };

    sort(vct.pocni(), vct.kraj()); //sortiramo od pocetka do kraja

    vct.insertiraj(vct.pocni(), 0); //insertiramo nulu na pocetak

    int suma = accumulate(vct.pocni(), vct.kraj(), 0); 

    vct.insertiraj(vct.kraj(), suma); // insertiramo sumu na kraj 

    for (int i = 0; i < vct.velicina(); i++) {
        cout << vct[i] << " ";
    }
}