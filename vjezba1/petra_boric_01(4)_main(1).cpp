#include <iostream>
using namespace std;
#include "header.h"

int main(){
    Vector m;
    m.vector_new(4);
    int a;
    cout << "Unesi elements. Za izaći, upiši 0;" << endl;
    cin >> a;
    while (a != 0){
        m.vector_push_back(a);
        cin >> a;
    }

    m.print_vector();
    cout << "Prvi element: " << m.vector_front() << endl;           
    cout << "Zadnji element " << m.vector_back() << endl;             

    cout << "Velicina " << m.vector_velicina() << endl;                     
    cout << "Capacity " << m.capacity << endl;

    cout << "Zadnji element uklonjen: " << endl;                         

    m.vector_pop_back();
    m.print_vector();
    m.vector_delete();
}