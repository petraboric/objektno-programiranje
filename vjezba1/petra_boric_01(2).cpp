using namespace std;
#include <iostream>

void ispis(int* niz, int a);
int* izbacivanje(int* niz, int n,int &novi_a_r);
int* unos(int* niz, int a);

int* unos(int* niz, int a){
	for (int i = 0; i < a; i++){
		cout << "unesi " << i + 1<<". element"<<endl;
		cin >> niz[i];
	}
	return niz;
}

void ispis(int* niz, int a){
	for (int i = 0; i < a; i++){
		cout << niz[i]<<endl;
	}
}

int* izbacivanje(int* niz, int a,int &aa){
	for (int i = 0; i < a - 1; i++){
		for (int j = i + 1; j < a; j++){
			if (niz[i] == niz[j]){
				for (int k = j; k < a; k++)
				{
					niz[k] = niz[k + 1];
				}
				a = a - 1;
			}
		}
	}
	aa = a;
	return niz;
}

int main() {
	int a;
	cout << "unesite broj elem. niza" << endl;
	cin >> a;
	int novi_a;
	int& novi_a_r = novi_a;
	int* niz = new int[a];

	niz = unos(niz, a);
	niz=izbacivanje(niz, a, novi_a_r);

	cout << novi_a << endl;

	ispis(niz, novi_a);
}
