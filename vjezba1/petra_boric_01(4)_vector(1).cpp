using namespace std;
#include "header.h"

void Vector::vector_new(velicina_t vel) {
    arr = new int[vel];
    velicina = 0;
    capacity = 1;
}
void Vector::vector_delete() {
    delete[] arr;
}
void Vector::vector_push_back(int a) {
    if (velicina == capacity){
        capacity *= 2;
        int* arrN = new int[capacity];
        for (int i = 0; i < (capacity - 1); i++)
        {
            arrN[i] = arr[i];
        }
        delete[] arr;
        arr = arrN;
    }
    arr[velicina] = a;
    velicina++;
}
void Vector::vector_pop_back() {
    velicina -= 1;
}
size_t Vector::vector_velicina() {
    return velicina;
}
int& Vector::vector_back() {
    return arr[velicina - 1];
}
int& Vector::vector_front() {
    return arr[0];
}
void Vector::print_vector(){
    for (velicina_t i = 0; i < vector_velicina(); i++)
        cout << arr[i] << " ";
    cout << endl;
}