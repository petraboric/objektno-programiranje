#include <iostream>

class Point {
	double x, y, z;

	void input();
	void postaviPseudVrijednost();
	void dotprint();
	void udaljenost2D();
	void udaljenost3D();
};
class Weapon {
	Point position;
	int trenutnoMetaka;
	int maxMetaka;
	void Reload();
	void Shoot();
}
