#include <iostream>
#include "header.h"
#include <random>
using namespace std;

Point::Point() {
	x = 0;
	y = 0;
	z = 0;
}
void Point::postaviTocke(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
void Point::postaviPseudVrijednost() {
	double ll, ul;
	cout << "Lower interval limit: " << endl;
	cin >> ll;
	cout << "Upper interval limit: " << endl;
	cin >> ul;
	srand(time(NULL))

		x = ll + (rand()) / ((RAND_MAX / (ul - ll)));                     //z visina yix polozaj (3D)
	y = ll + (rand()) / ((RAND_MAX / (ul - ll)));
	z = ll + (rand()) / ((RAND_MAX / (ul - ll)));
}
void Point::dohvacanjeVrijednostiClanova(double& x, double& y, double& z) const {
	x = this->x;            //iz klase
	y = this->y;
	z = this->z;
}
float Point::udaljenost2D(Point& first, Point& second) const {   //formula iz matematike
	return sqrt(pow((second.x - first.x), 2) + pow((second.y - first.y), 2));
}
float Point::udaljenost3D(Point& first, Point& second) const {
	sqrt(pow((second.x - first.x), 2) + pow((second.y - first.y), 2) + pow((second.z - first.z), 2));
}


bool Meta::pogodenaMeta(Weapon weapon, Point llf, Point urb) {                 //jeli visina puske izmedu gornje i dojnje mete
	return (weapon.position.z >= llf.z && weapon.position.z <= urb.z);
}
