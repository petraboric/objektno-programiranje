#include <iostream>
#include "header.h"
#include <random>
using namespace std;

int main() {
	srand(time(NULL));
	Weapon weapon;         //inicijalizacija
	Point prviRandom, drugiRandom;
	int brojmeta;
	bool targethit = false;

	cout << "Unesi broj meta: " << endl;
	cin >> brojmeta;

	Target* nizMeta = new Target[brojmeta];      //malloc
	for (int i = 0; i < brojmeta; i++) {
		prviRandom.postaviPseudVrijednost();
		drugiRandom.postaviPseudVrijednost();
		nizMeta[i].dohvacanjePozicija(prviRandom, drugiRandom);   //u niz(f) saljem kordinate
	}
	for (int i = 0; i < brojmeta; i++) {
		weapon.Shoot();
		if (nizMeta[i].pogodenaMeta(weapon, nizMeta[i].llf, nizMeta[i].urb) == true) {      //bool
			targethit = true;
			cout << "Target number [" << i + 1 << "] has been shot" << endl;
		}
		else {
			targethit = false;
			cout << "Target number [" << i + 1 << "] has not been shot" << endl;
		}
	}
	return 0;
}
