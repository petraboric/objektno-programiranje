#include <iostream>
#include <random>

class Point {
public:
	double x, y, z;
	void postaviTocke(double x, double y, double z);
	void postaviPseudVrijednost();
	void dohvacanjeVrijednostiClanova(double& x, double& y, double& z) const;
	float udaljenost2D(Point& first, Point& second) const;
	float udaljenost3D(Point& first, Point& second) const;
};
class Weapon {
public:
	Point position;
	int trenutnoMetaka = 30;
	int maxMetaka = 30;
	void Reload(void)
	{
		trenutnoMetaka = maxMetaka;
	}
	void Shoot(void)
	{
		--maxMetaka;
	}
};
class Meta {
public:
	Point llf;  //lower left front , lower right front...
	Point lrf;                                    //fri�ider(8)
	Point ulf;
	Point urf;

	Point llb;  //back
	Point lrb;
	Point ulb;
	Point urb;
	void dohvacanjePozicija(Point& lowerleft, Point& upperright) {
		double x1, y1, z1, x2, y2, z2;

		lowerleft.postaviTocke(x1, y1, z1);
		llf.dohvacanjeVrijednostiClanova(x1, y1, z1);

		upperright.postaviTocke(x2, y2, z2);
		urb.dohvacanjeVrijednostiClanova(x2, y2, z2);

		lrf.postaviTocke(x2, y1, z1);
		urf.postaviTocke(x2, y2, z2);
		ulf.postaviTocke(x1, z1, z2);

		llb.postaviTocke(x1, y2, z1);
		lrb.postaviTocke(x2, y2, z1);
		ulb.postaviTocke(x1, y2, z2);
	}
	bool pogodenaMeta(Weapon weapon, Point llf, Point urb);
};

