#include <iostream>
#include <vector>
#include <string>
using namespace std;
template <typename T>

class Point  {
	T x, y;
public:
	Point(T p1,T p2): x(p1),y(p2) {}

	friend ostream& operator<<(ostream& out, const Point& p) {
		return out << "(" << p.x << ", " << p.y << ")"; }

	friend float operator-(const Point& p1, const Point& p2) {
		float udaljenost= sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
		return udaljenost; }
};

int main() {
	Point <int> p1(8,4), b(9,4);
	cout << "Udaljenost je:" << p1 << " i " << p2 << " je " << p1 - p2;
}
