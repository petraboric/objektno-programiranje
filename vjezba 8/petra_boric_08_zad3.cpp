#include <iostream>
#include <vector>
#include <string>
using namespace std;
template <typename T> void sortiraj(T arr[], int n)  {
	for (int i = 0; i < n-1; ++i) {
		for (int j = i+1; j < n; ++j) {
			if (arr[i] < arr[j])
				swap(arr[i], arr[j]); }
	}
}

template <> void sortiraj <char>(char arr[], int n) {
	for (int i = 0; i < n-1; ++i) {
		for (int j = i+1; j < n; ++j) {
			if (tolower(arr[i]) > tolower(arr[j]))
				swap(arr[i], arr[j]); }
	}
}

int main() {
	int niz[] = { 85,26,7,7,90 };
	char c[] = { 'P','b','f','B'};

	int c_n = sizeof(c) / sizeof(c[0]);
	int niz_n = sizeof(niz) / sizeof(niz[0]);
	
	sortiraj (niz, niz_n);
	sortiraj (c, c_n);

	for (int i = 0; i < sizeof(niz) / sizeof(niz[0]); ++i)
		cout << niz[i] << " ";
	cout << endl;
	for (int i = 0; i < sizeof(c) / sizeof(c[0]); ++i)
		cout << c[i] << " ";    }