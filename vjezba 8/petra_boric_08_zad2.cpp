#include <iostream>
#include <vector>
#include <string>
using namespace std;

template <typename T>
class skup {
	 T* niz;
    int size;
    int count;

public:
    skup(int size) : size(size), niz(new T[size]) {}

    void dodavanje(T element) {
        if (count >= size) {
            T* novi = new T[size * 2];
            for (int i = 0; i < count; i++) {
                novi[i] = niz[i]; }
            delete[] niz;
            niz = novi;
            size *= 2;
        }
        niz[count] = element;
        count++; }

	 void izbacivanje(int broj) {
        int index = -1;
        for (int i = 0; i < count; i++) {
            if (niz[i] == broj) {
                index = i;
                break; }
        }
        if (index == -1)
            return;
        for (int i = index; i < count - 1; i++) {
            niz[i] = niz[i + 1];  }
        count--; }

     bool provjera (int num)  {
        for (int i = 0; i < count-1; i++) {
            if (num == niz[i])
                return 1; }
        return 0;
    }

    void printaj(){
        for (int i = 0; i <= count-1 ; i++) {
            cout << niz[i] << " ";  }
        cout << endl;
    }
	
	 ~ skup () {
        delete[] niz;
    }
};

int main() {
    skup <int> a(100);       
    a.dodavanje();           //dodaji skup i u funkciji koji br pregledajes
    a.dodavanje();
    a.dodavanje();
	a.izbacivanje();
    a.dodavanje();
    a.dodavanje();
    a.dodavanje();
	a.izbacivanje();
    a.printaj();

    if (a.provjera()==0)
        cout << "Nije u nizu" << endl;
    else
        cout << "U nizu je" << endl;  }